/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_javascript_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./src/javascript/app */ "./src/javascript/app.js");
/* harmony import */ var _src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./src/styles/styles.css */ "./src/styles/styles.css");
/* harmony import */ var _src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1__);


new _src_javascript_app__WEBPACK_IMPORTED_MODULE_0__["default"]();

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css":
/*!*********************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, "html,\nbody {\n    height: 100%;\n    width: 100%;\n    margin: 0;\n    padding: 0;\n}\n\n#root {\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: center;\n    height: 100%;\n    width: 100%;\n}\n\n.fighters {\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    flex: 1;\n    flex-wrap: wrap;\n    padding: 0 15px;\n}\n\n.fighter {\n    display: flex;\n    flex-direction: column;\n    padding: 20px;\n}\n\n.fighter:hover {\n    box-shadow: 0 0 50px 10px rgba(0,0,0,0.06);\n    cursor: pointer;\n}\n\n.name {\n    align-self: center;\n    font-size: 21px;\n    margin-top: 20px;\n}\n\n.fighter-image {\n    height: 260px;\n}\n\n#loading-overlay {\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    font-size: 18px;\n    background: rgba(255, 255, 255, 0.7);\n    visibility: hidden;\n}\n\n.btn {\n    display: inline-block;\n    background-color: #ddd;\n    color: #000;\n    padding: 8px;\n    border: 0;\n    white-space: nowrap;\n    vertical-align: middle;\n    text-align: center;\n    text-decoration: none;\n    font-size: 14px;\n    font-weight: 400;\n    cursor: pointer;\n    border-radius: 3px;\n    margin: 0 3px 8px 0;\n}\n.btnPrimary {\n    background-color: rgb(161, 5, 252)\n}\n\n.modal {\n    display: none;\n    position: fixed;\n    z-index: 1;\n    padding-top: 100px;\n    left: 0;\n    top: 0;\n    width: 100%;\n    height: 100%;\n    overflow: auto;\n    background-color: rgb(0,0,0);\n    background-color: rgba(0,0,0,0.4);\n}\n\n.modal-content {\n    background-color: #fefefe;\n    margin: auto;\n    padding: 20px;\n    border: 1px solid #888;\n    width: 15%;\n}\ndiv.container {\n    height: 10em;\n    position: relative;\n} \nbutton.start-a-fight {\n    margin: 0;\n    position: absolute;\n    top: 10%;         \n    transform: translate(0, -50%);\n    display: inline-block;\n    color: black;\n    font-size: 125%;\n    font-weight: 700;\n    text-decoration: none;\n    user-select: none;\n    padding: .25em .5em;\n    outline: none;\n    border: 1px solid rgb(255, 22, 22);\n    border-radius: 7px;\n    background: rgb(240, 2, 2);\n    box-shadow: inset 0 -2px 1px rgba(0,0,0,0), inset 0 1px 2px rgba(0,0,0,0), inset 0 0 0 60px rgba(255,255,0,0);\n    transition: box-shadow .2s, border-color .2s;\n}\n\nbutton.start-a-fight:hover {\n   box-shadow: inset 0 -1px 1px rgba(0,0,0,0), inset 0 1px 2px rgba(0,0,0,0), inset 0 0 0 60px rgba(255,255,0,.5);\n}\nbutton.start-a-fight:active {\n    padding: calc(.25em + 1px) .5em calc(.25em - 1px);\n    border-color: rgb(173, 156, 0);\n    box-shadow: inset 0 -1px 1px rgba(0,0,0,.1), inset 0 1px 2px rgba(0,0,0,.3), inset 0 0 0 60px rgba(255,255,0,.45);\n}\n\n.result{\n  top: 3%;\n  left: 3%;\n  width: 7%;\n  position: fixed;\n  background-color: white;\n  visibility: hidden;\n\n}", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return '@media ' + item[2] + '{' + content + '}';
      } else {
        return content;
      }
    }).join('');
  }; // import a list of modules into the list


  list.i = function (modules, mediaQuery) {
    if (typeof modules === 'string') {
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    for (var i = 0; i < this.length; i++) {
      var id = this[i][0];

      if (id != null) {
        alreadyImportedModules[id] = true;
      }
    }

    for (i = 0; i < modules.length; i++) {
      var item = modules[i]; // skip already imported module
      // this implementation is not 100% perfect for weird media query combinations
      // when a module is imported multiple times with different media queries.
      // I hope this will never occur (Hey this way we have smaller bundles)

      if (item[0] == null || !alreadyImportedModules[item[0]]) {
        if (mediaQuery && !item[2]) {
          item[2] = mediaQuery;
        } else if (mediaQuery) {
          item[2] = '(' + item[2] + ') and (' + mediaQuery + ')';
        }

        list.push(item);
      }
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || '';
  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */';
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;
  return '/*# ' + data + ' */';
}

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/javascript/app.js":
/*!*******************************!*\
  !*** ./src/javascript/app.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _views_fightersView__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./views/fightersView */ "./src/javascript/views/fightersView.js");
/* harmony import */ var _services_fightersService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/fightersService */ "./src/javascript/services/fightersService.js");
/* harmony import */ var _fight_startFight__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fight/startFight */ "./src/javascript/fight/startFight.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





class App {
  constructor() {
    this.startApp();
    Object(_fight_startFight__WEBPACK_IMPORTED_MODULE_2__["default"])();
  }

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      const fighters = await _services_fightersService__WEBPACK_IMPORTED_MODULE_1__["fighterService"].getFighters();
      const fightersView = new _views_fightersView__WEBPACK_IMPORTED_MODULE_0__["default"](fighters);
      const fightersElement = fightersView.element;
      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }

}

_defineProperty(App, "rootElement", document.getElementById('root'));

_defineProperty(App, "loadingElement", document.getElementById('loading-overlay'));

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/javascript/fight/fight.js":
/*!***************************************!*\
  !*** ./src/javascript/fight/fight.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return fight; });
/* harmony import */ var _fightersDetailsMap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fightersDetailsMap */ "./src/javascript/fight/fightersDetailsMap.js");
/* harmony import */ var _fighter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fighter */ "./src/javascript/fight/fighter.js");
/* harmony import */ var _fightersInBattle__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fightersInBattle */ "./src/javascript/fight/fightersInBattle.js");
/* harmony import */ var _views_isFighting__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../views/isFighting */ "./src/javascript/views/isFighting.js");






let flag = true;
function fight(firstId, secondId) {
  _views_isFighting__WEBPACK_IMPORTED_MODULE_3__["default"][0] = true;
  let firstDetails = _fightersDetailsMap__WEBPACK_IMPORTED_MODULE_0__["default"].get(firstId);
  let secondDetails = _fightersDetailsMap__WEBPACK_IMPORTED_MODULE_0__["default"].get(secondId);
  document.getElementById('result-name-1').value = firstDetails.name;
  document.getElementById('result-health-1').value = Math.round(firstDetails.health);
  document.getElementById('result-name-2').value = secondDetails.name;
  document.getElementById('result-health-2').value = Math.round(secondDetails.health);
  document.getElementById('result').style.visibility = 'visible';

  const hit = (first, second) => {
    const damage = _fighter__WEBPACK_IMPORTED_MODULE_1__["fighter"].getHitPower(first) - _fighter__WEBPACK_IMPORTED_MODULE_1__["fighter"].getBlockPower(second);
    return damage >= 0 ? damage : 0;
  }; //determining who will beat now
  // true - first fighter


  if (flag) {
    turn(firstDetails, secondDetails, firstId, secondId, hit);
  } else {
    turn(secondDetails, firstDetails, secondId, firstId, hit);
  }
}

async function turn(firstDetails, secondDetails, firstId, secondId, hit) {
  flag = !flag;
  secondDetails.health -= hit(firstId, secondId);

  if (secondDetails.health <= 0) {
    _views_isFighting__WEBPACK_IMPORTED_MODULE_3__["default"][0] = false;
    _fightersInBattle__WEBPACK_IMPORTED_MODULE_2__["default"].forEach(f => {
      const fighterElement = document.getElementsByClassName('fighter')[+f - 1];
      fighterElement.style.backgroundColor = 'rgb(255, 255, 255)';
      document.getElementById('start-a-fight').textContent = 'Start a fight';
      document.getElementById('result').style.visibility = 'hidden';
    });
    _fightersInBattle__WEBPACK_IMPORTED_MODULE_2__["default"].length = 0;
    alert(`${firstDetails.name} won`);
  }
}

/***/ }),

/***/ "./src/javascript/fight/fighter.js":
/*!*****************************************!*\
  !*** ./src/javascript/fight/fighter.js ***!
  \*****************************************/
/*! exports provided: fighter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fighter", function() { return fighter; });
/* harmony import */ var _fightersDetailsMap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fightersDetailsMap */ "./src/javascript/fight/fightersDetailsMap.js");


class Fighter {
  constructor(name, health, attack, defense) {
    this.name = name;
    this.health = health;
    this.attack = attack;
    this.defense = defense;
    this.getHitPower = this.getHitPower.bind(this);
    this.getBlockPower = this.getBlockPower.bind(this);
  }

  getHitPower(fighterId) {
    const {
      attack
    } = _fightersDetailsMap__WEBPACK_IMPORTED_MODULE_0__["default"].get(fighterId);
    const criticalHitChance = Math.random() + 1;
    const power = attack * criticalHitChance;
    return power;
  }

  getBlockPower(fighterId) {
    const {
      defense
    } = _fightersDetailsMap__WEBPACK_IMPORTED_MODULE_0__["default"].get(fighterId);
    const dodgeChance = Math.random() + 1;
    const power = defense * dodgeChance;
    return power;
  }

}

const fighter = new Fighter();

/***/ }),

/***/ "./src/javascript/fight/fightersDetailsMap.js":
/*!****************************************************!*\
  !*** ./src/javascript/fight/fightersDetailsMap.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const fightersDetailsMap = new Map();
/* harmony default export */ __webpack_exports__["default"] = (fightersDetailsMap);

/***/ }),

/***/ "./src/javascript/fight/fightersInBattle.js":
/*!**************************************************!*\
  !*** ./src/javascript/fight/fightersInBattle.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const fightersInBattle = [];
/* harmony default export */ __webpack_exports__["default"] = (fightersInBattle);

/***/ }),

/***/ "./src/javascript/fight/startFight.js":
/*!********************************************!*\
  !*** ./src/javascript/fight/startFight.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return startFight; });
/* harmony import */ var _fight__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fight */ "./src/javascript/fight/fight.js");
/* harmony import */ var _fightersInBattle__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fightersInBattle */ "./src/javascript/fight/fightersInBattle.js");


async function startFight() {
  document.getElementById('start-a-fight').addEventListener('click', () => {
    if (_fightersInBattle__WEBPACK_IMPORTED_MODULE_1__["default"].length < 2) {
      alert('You need two fighters to fight');
    } else {
      document.getElementById('start-a-fight').textContent = 'Hit as hard as you can';
      Object(_fight__WEBPACK_IMPORTED_MODULE_0__["default"])(..._fightersInBattle__WEBPACK_IMPORTED_MODULE_1__["default"]);
    }
  });
}

/***/ }),

/***/ "./src/javascript/helpers/apiHelper.js":
/*!*********************************************!*\
  !*** ./src/javascript/helpers/apiHelper.js ***!
  \*********************************************/
/*! exports provided: callApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "callApi", function() { return callApi; });
const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';

function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  const options = {
    method
  };
  return fetch(url, options).then(response => response.ok ? response.json() : Promise.reject(Error('Failed to load'))).catch(error => {
    throw error;
  });
}



/***/ }),

/***/ "./src/javascript/services/fightersService.js":
/*!****************************************************!*\
  !*** ./src/javascript/services/fightersService.js ***!
  \****************************************************/
/*! exports provided: fighterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fighterService", function() { return fighterService; });
/* harmony import */ var _helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/apiHelper */ "./src/javascript/helpers/apiHelper.js");


class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await Object(_helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__["callApi"])(endpoint, 'GET');
      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const endpoint = `details/fighter/${_id}.json`;
      const apiResult = await Object(_helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__["callApi"])(endpoint, 'GET');
      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

}

const fighterService = new FighterService();

/***/ }),

/***/ "./src/javascript/views/coloring.js":
/*!******************************************!*\
  !*** ./src/javascript/views/coloring.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ((fightersInBattle, fighter) => {
  let flag = true;

  if (fightersInBattle.includes(fighter._id)) {
    const index = fightersInBattle.indexOf(fighter._id);
    const removedFighter = fightersInBattle[index] - 1;
    const fighterElement = document.getElementsByClassName('fighter')[removedFighter];
    fighterElement.style.backgroundColor = 'rgb(255, 255, 255)';

    if (index === 0) {
      fightersInBattle.shift();
    } else {
      fightersInBattle.pop();
    }

    flag = false;
  }

  if (fightersInBattle.length < 2 && flag === true) {
    const fighterElement = document.getElementsByClassName('fighter')[+fighter._id - 1];
    fightersInBattle.push(fighter._id);
    fighterElement.style.backgroundColor = 'rgba(255, 0, 0, 0.9)';
    flag = false;
  }

  if (fightersInBattle.includes(fighter._id) && flag === true) {
    const index = fightersInBattle.indexOf(fighter._id);
    const removedFighter = fightersInBattle[index] - 1;
    const fighterElement = document.getElementsByClassName('fighter')[removedFighter];
    fighterElement.style.backgroundColor = 'rgb(255, 255, 255)';
    fightersInBattle.shift();
  }
});

/***/ }),

/***/ "./src/javascript/views/fighterView.js":
/*!*********************************************!*\
  !*** ./src/javascript/views/fighterView.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./src/javascript/views/view.js");


class FighterView extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(fighter, handleClick, handleDoubleClick) {
    super();
    this.createFighter(fighter, handleClick, handleDoubleClick);
  }

  createFighter(fighter, handleClick, handleDoubleClick) {
    const {
      name,
      source
    } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    this.element = this.createElement({
      tagName: 'div',
      className: 'fighter'
    });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
    this.element.addEventListener('dblclick', event => handleDoubleClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: 'span',
      className: 'name'
    });
    nameElement.innerText = name;
    return nameElement;
  }

  createImage(source) {
    const attributes = {
      src: source
    };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });
    return imgElement;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FighterView);

/***/ }),

/***/ "./src/javascript/views/fightersView.js":
/*!**********************************************!*\
  !*** ./src/javascript/views/fightersView.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./src/javascript/views/view.js");
/* harmony import */ var _fighterView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fighterView */ "./src/javascript/views/fighterView.js");
/* harmony import */ var _services_fightersService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/fightersService */ "./src/javascript/services/fightersService.js");
/* harmony import */ var _fight_fightersDetailsMap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../fight/fightersDetailsMap */ "./src/javascript/fight/fightersDetailsMap.js");
/* harmony import */ var _fight_fightersInBattle__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../fight/fightersInBattle */ "./src/javascript/fight/fightersInBattle.js");
/* harmony import */ var _coloring__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./coloring */ "./src/javascript/views/coloring.js");
/* harmony import */ var _modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modal */ "./src/javascript/views/modal.js");
/* harmony import */ var _isFighting__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./isFighting */ "./src/javascript/views/isFighting.js");









class FightersView extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(fighters) {
    super();
    this.handleClick = this.handleFighterClick.bind(this);
    this.handleDoubleClick = this.handleDoubleClick.bind(this);
    this.createFighters(fighters);
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new _fighterView__WEBPACK_IMPORTED_MODULE_1__["default"](fighter, this.handleClick, this.handleDoubleClick);
      return fighterView.element;
    });
    this.element = this.createElement({
      tagName: 'div',
      className: 'fighters'
    });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    if (!_fight_fightersDetailsMap__WEBPACK_IMPORTED_MODULE_3__["default"].get(fighter._id)) {
      const details = await _services_fightersService__WEBPACK_IMPORTED_MODULE_2__["fighterService"].getFighterDetails(fighter._id);
      _fight_fightersDetailsMap__WEBPACK_IMPORTED_MODULE_3__["default"].set(fighter._id, details);
    }

    ;

    if (!_isFighting__WEBPACK_IMPORTED_MODULE_7__["default"][0]) {
      Object(_coloring__WEBPACK_IMPORTED_MODULE_5__["default"])(_fight_fightersInBattle__WEBPACK_IMPORTED_MODULE_4__["default"], fighter);
    } else {
      alert('Please finish the current fight');
    }
  }

  async handleDoubleClick(event, fighter) {
    if (!_fight_fightersDetailsMap__WEBPACK_IMPORTED_MODULE_3__["default"].get(fighter._id)) {
      const details = await _services_fightersService__WEBPACK_IMPORTED_MODULE_2__["fighterService"].getFighterDetails(fighter._id);
      _fight_fightersDetailsMap__WEBPACK_IMPORTED_MODULE_3__["default"].set(fighter._id, details);
    }

    if (!_isFighting__WEBPACK_IMPORTED_MODULE_7__["default"][0]) {
      Object(_modal__WEBPACK_IMPORTED_MODULE_6__["default"])(fighter, _fight_fightersDetailsMap__WEBPACK_IMPORTED_MODULE_3__["default"]);
    } else {
      alert('Please finish the current fight');
    }
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FightersView);

/***/ }),

/***/ "./src/javascript/views/isFighting.js":
/*!********************************************!*\
  !*** ./src/javascript/views/isFighting.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
let isFighting = [false];
/* harmony default export */ __webpack_exports__["default"] = (isFighting);

/***/ }),

/***/ "./src/javascript/views/modal.js":
/*!***************************************!*\
  !*** ./src/javascript/views/modal.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const modal = (fighter, fightersDetailsMap) => {
  const details = fightersDetailsMap.get(fighter._id);
  const inputs = [];
  const form = document.getElementById('details-form');

  function complete(details) {
    fightersDetailsMap.set(fighter._id, details);
  }

  ;

  for (const key in details) {
    const element = document.getElementById(key);

    if (element) {
      element.value = details[key];
      inputs.push(element);
    }
  }

  ;

  form.onsubmit = function () {
    console.log('df' + inputs);
    inputs.forEach(input => details[input.id] = input.value);
    complete(details);
    console.log(details);
    return false;
  };

  const modal = document.getElementById('myModal');
  const close = document.querySelectorAll('[data-close="true"]');
  modal.style.display = 'block';

  for (let i = 0; i < close.length; i++) {
    close[i].onclick = function () {
      modal.style.display = 'none';
    };
  }

  document.onkeydown = function (e) {
    if (e.keyCode == 27) {
      modal.style.display = 'none';
    }

    ;
  };
};

/* harmony default export */ __webpack_exports__["default"] = (modal);

/***/ }),

/***/ "./src/javascript/views/view.js":
/*!**************************************!*\
  !*** ./src/javascript/views/view.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class View {
  constructor() {
    _defineProperty(this, "element", void 0);
  }

  createElement({
    tagName,
    className = '',
    attributes = {}
  }) {
    const element = document.createElement(tagName);
    element.classList.add(className);
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
    return element;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (View);

/***/ }),

/***/ "./src/styles/styles.css":
/*!*******************************!*\
  !*** ./src/styles/styles.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js!./styles.css */ "./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map