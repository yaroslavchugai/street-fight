import fight from './fight';
import fightersInBattle from './fightersInBattle';

export default async function startFight (){
  document.getElementById('start-a-fight').addEventListener('click', () => {
    if (fightersInBattle.length < 2) {
      alert('You need two fighters to fight');
    } else {
      document.getElementById('start-a-fight').textContent = 'Hit as hard as you can'
      fight(...fightersInBattle);
    }
  });
}

