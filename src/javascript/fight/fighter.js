import fightersDetailsMap from './fightersDetailsMap';

class Fighter {
  constructor(name, health, attack, defense) {
    this.name = name;
    this.health = health;
    this.attack = attack;
    this.defense = defense;
    this.getHitPower = this.getHitPower.bind(this);
    this.getBlockPower = this.getBlockPower.bind(this);
  }

  getHitPower(fighterId) {
    const { attack } = fightersDetailsMap.get(fighterId);
    const criticalHitChance = Math.random() + 1;
    const power = attack * criticalHitChance;
    return power;
  }
  getBlockPower(fighterId) {
    const { defense } = fightersDetailsMap.get(fighterId);
    const dodgeChance = Math.random() + 1;
    const power = defense * dodgeChance;
    return power;
  }
}

export const fighter = new Fighter();