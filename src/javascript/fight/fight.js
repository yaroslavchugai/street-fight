'use strict';

import fightersDetailsMap from './fightersDetailsMap';
import { fighter } from './fighter';
import fightersInBattle from './fightersInBattle';
import isFighting from '../views/isFighting';


let flag = true;

export default function fight(firstId, secondId) {
  isFighting[0] = true;

  let firstDetails = fightersDetailsMap.get(firstId);
  let secondDetails = fightersDetailsMap.get(secondId);

  document.getElementById('result-name-1').value = firstDetails.name;
  document.getElementById('result-health-1').value = Math.round(firstDetails.health)
  document.getElementById('result-name-2').value = secondDetails.name;
  document.getElementById('result-health-2').value = Math.round(secondDetails.health);
  document.getElementById('result').style.visibility = 'visible';

  const hit = (first, second) => {
    const damage = fighter.getHitPower(first) - fighter.getBlockPower(second);
    return damage >= 0 ? damage : 0;
  };

  //determining who will beat now
  // true - first fighter

  if(flag) {
    turn(firstDetails, secondDetails, firstId, secondId, hit);
  } else {
    turn(secondDetails, firstDetails, secondId, firstId, hit);
  }
}

async function turn(firstDetails, secondDetails, firstId, secondId, hit) {
  flag = !flag;
  secondDetails.health -= hit(firstId, secondId);
  if (secondDetails.health <= 0 ) {
    isFighting[0] = false;
    fightersInBattle.forEach(f => {
      const fighterElement = document.getElementsByClassName('fighter')[+f - 1];
      fighterElement.style.backgroundColor = 'rgb(255, 255, 255)';
      document.getElementById('start-a-fight').textContent = 'Start a fight';
      document.getElementById('result').style.visibility = 'hidden';

    });
    fightersInBattle.length = 0;

    alert(`${firstDetails.name} won`);
  }
}