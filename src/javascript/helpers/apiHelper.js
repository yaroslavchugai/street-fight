const API_URL = 'http://localhost:3000/';

function callApi(endpoind, method, body) {
  const url = API_URL + endpoind;
  
  const myHeaders = new Headers({
    "Authorization": "admin"
  });
  
  const options = { 
    method,
    headers: myHeaders,
    body
  };

  return fetch(url, options)
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    )
    
    .catch(error => {
      throw error;
    });
}

export { callApi }