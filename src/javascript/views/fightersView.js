import View from './view';
import FighterView from './fighterView';
import { fighterService } from '../services/fightersService';
import fightersDetailsMap from '../fight/fightersDetailsMap';
import fightersInBattle from '../fight/fightersInBattle';
import coloring from './coloring';
import modal from './modal';
import isFighting from './isFighting';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.handleDoubleClick = this.handleDoubleClick.bind(this);
    this.createFighters(fighters);
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick, this.handleDoubleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    if(!fightersDetailsMap.get(fighter._id)) {
      const details = await fighterService.getFighterDetails(fighter._id);
      fightersDetailsMap.set(fighter._id, details);
    };
    if(!isFighting[0]){
      coloring(fightersInBattle, fighter);
    } else {
      alert('Please finish the current fight')
    }
    
  }

  async handleDoubleClick(event, fighter) {
    if(!fightersDetailsMap.get(fighter._id)) {
      const details = await fighterService.getFighterDetails(fighter._id);
      fightersDetailsMap.set(fighter._id, details);
    }
    if(!isFighting[0]){
      modal(fighter, fightersDetailsMap)
    } else {
      alert('Please finish the current fight')
    }
  }
  
}

export default FightersView;