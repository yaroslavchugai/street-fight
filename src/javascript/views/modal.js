import { fighterService } from '../services/fightersService';

const modal = (fighter, fightersDetailsMap) => {
  const details = fightersDetailsMap.get(fighter._id);
  const inputs = [];
  const form = document.getElementById('details-form');
  

  function complete(details) {
    fightersDetailsMap.set(fighter._id, details);
    console.log(details);

  };

  for (const key in details) {
    const element = document.getElementById(key);
    if (element) {
      element.value = details[key];
      inputs.push(element);
    }
  };

  form.onsubmit = function() {
    inputs.forEach(input => details[input.id] = input.value);
    complete(details);
    return false;
  };

  const modal = document.getElementById('myModal');
  const close = document.querySelectorAll('[data-close="true"]');
  modal.style.display = 'block';

  for (let i = 0; i < close.length; i++) {
    close[i].onclick = function() {
      modal.style.display = 'none';
    };
  }

  document.onkeydown = function(e) {
    if (e.keyCode == 27) {
      modal.style.display = 'none';
    };
  };
};

export default modal;


