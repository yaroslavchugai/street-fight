export default (fightersInBattle, fighter) => {
  let flag = true;

    if(fightersInBattle.includes(fighter._id)){
      const index = fightersInBattle.indexOf(fighter._id)
      const removedFighter = fightersInBattle[index] - 1;
      const fighterElement = document.getElementsByClassName('fighter')[removedFighter];
      fighterElement.style.backgroundColor = 'rgb(255, 255, 255)';
      if(index === 0){

        fightersInBattle.shift();
      } else {
        fightersInBattle.pop();
      }
      flag = false;
    }

    if(fightersInBattle.length < 2  && flag === true ) {
      const fighterElement = document.getElementsByClassName('fighter')[+fighter._id - 1];
      fightersInBattle.push(fighter._id);
      fighterElement.style.backgroundColor = 'rgba(255, 0, 0, 0.9)';
      flag = false;
    }

    if(fightersInBattle.includes(fighter._id) && flag === true ){
      const index = fightersInBattle.indexOf(fighter._id);
      const removedFighter = fightersInBattle[index] - 1;
      const fighterElement = document.getElementsByClassName('fighter')[removedFighter];
      fighterElement.style.backgroundColor = 'rgb(255, 255, 255)';
      fightersInBattle.shift();
    }
}